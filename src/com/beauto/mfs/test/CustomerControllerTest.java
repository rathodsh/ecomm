package com.beauto.mfs.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.beauto.mfs.request.model.Corporate;
import com.beauto.mfs.request.model.GrossBillAmount;
import com.beauto.mfs.request.model.NetBillAmount;
import com.beauto.mfs.request.model.PaymentRequestModel;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({ "classpath*:spring/applicationContext.xml" })
public class CustomerControllerTest {

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
		this.mockMvc = builder.build();
	}

	@Test
    public void testUserController () throws Exception {
        ResultMatcher ok = MockMvcResultMatchers.status()
                                                .isOk();
        
        PaymentRequestModel payment = new PaymentRequestModel("sghsvddbd", "123abc", 0.23, "abcfz123gh", "9834102440",
        		new GrossBillAmount(2345.86, "8214"), new NetBillAmount(2376.88, "123sbc"),
        		new Corporate("codecop", "anc"), null);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/");
        ResultActions onj = this.mockMvc.perform(builder)
                    .andExpect(ok);
       
       System.out.println("Hiiii");

    }
}