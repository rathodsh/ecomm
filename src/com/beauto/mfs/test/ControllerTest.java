package com.beauto.mfs.test;

import java.util.Date;

import javax.net.ssl.SSLEngineResult.Status;

import org.hibernate.validator.internal.constraintvalidators.bv.AssertTrueValidator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.beauto.mfs.request.controller.PaymentRequestControllerImpl;
import com.beauto.mfs.request.model.Corporate;
import com.beauto.mfs.request.model.GrossBillAmount;
import com.beauto.mfs.request.model.NetBillAmount;
import com.beauto.mfs.request.model.PaymentRequestModel;
import com.beauto.mfs.request.model.SenderInfo;
import com.beauto.mfs.request.service.PaymentRequestService;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath*:spring/applicationContext.xml" })
public class ControllerTest {

	private MockMvc mockMvc;

	@Mock
	private PaymentRequestService userService;

	@InjectMocks
	private PaymentRequestControllerImpl userController;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
	}

	@Test
	public void requestPay() throws Exception {

		SenderInfo sender = new SenderInfo("abc", "jdhb", new Date(), "ubudddu", "djgbwjdw", "jcbuhdj", new Date(),
				"dufiwjflw", "dhbwdj", "hbcbd", "duwud", "djgbdujw", "dggbwjdw", "dhbwudwj");

		PaymentRequestModel model = new PaymentRequestModel("sghsvddbd", "123abc", 0.23, "abcfz123gh", "9834102440",
				new GrossBillAmount(2345.86, "8214"), new NetBillAmount(2376.88, "123sbc"),
				new Corporate("codecop", "anc"), sender);

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/payment/request");
		
		 MvcResult onj = this.mockMvc.perform(builder)
                .andReturn();
		 
		int status = onj.getResponse().getStatus();
		
		ResponseEntity<Object> responseEntity = userController.requestPayment(model);
		System.out.println(responseEntity.getStatusCode());
		Assert.assertEquals(responseEntity.getStatusCode().value(), 201);

	}

	// private MockMvc mockMvc;
	//
	// ObjectMapper ojectmapper = new ObjectMapper();
	//
	// @Autowired
	// private WebApplicationContext webApplicationContext;
	//
	// @Before
	// public void setup(){
	// mockMvc =
	// MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	// }
	//
	// @Test
	// public void paymentReq() throws JsonProcessingException{
	//
	// PaymentRequestModel payment = new PaymentRequestModel("sghsvddbd",
	// "123abc", 0.23, "abcfz123gh", "9834102440",
	// new GrossBillAmount(2345.86, "8214"), new NetBillAmount(2376.88,
	// "123sbc"),
	// new Corporate("codecop", "anc"), null);
	//
	// String jsonReq = ojectmapper.writeValueAsString(payment);
	//
	// //mockMvc.perform(post("").content(jsonReq).content(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
	//
	// }
	//
	//
	//// @Autowired
	//// @InjectMocks
	//// private PaymentRequestControllerImpl helloWorldController;
	////
	//// @Mock
	//// private PaymentRequestService service;
	////
	//// @Before
	//// public void setup() {
	//// MockitoAnnotations.initMocks(this);
	//// //this.service =
	// MockMvcBuilders.standaloneSetup(helloWorldController).build();
	//// System.out.println("hello");
	//// }
	////
	//// @Test
	//// public void testCreateSignupFormInvalidUser() throws Exception {
	////// this.mockMvc.perform(get("/")).andExpect(status().isOk());
	////// mockMvc.
	//// }
	//
	//// @Mock
	//// PaymentRequestService mockMvc;
	////
	//// @InjectMocks
	//// private PaymentRequestControllerImpl requestTest;
	////
	//// @Test
	//// public void testCreateSignupFormInvalidUser() throws Exception {
	//// MockHttpServletRequest request = new MockHttpServletRequest();
	//// RequestContextHolder.setRequestAttributes(new
	// ServletRequestAttributes(request));
	////
	//// PaymentRequestModel payment = new PaymentRequestModel("sghsvddbd",
	// "123abc", 0.23, "abcfz123gh", "9834102440",
	//// new GrossBillAmount(2345.86, "8214"), new NetBillAmount(2376.88,
	// "123sbc"),
	//// new Corporate("codecop", "anc"), null);
	////
	//// requestTest.requestPayment(payment);
	////
	//// }
	//
}
