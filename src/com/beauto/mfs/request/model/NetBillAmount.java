package com.beauto.mfs.request.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Setter
@Getter
public class NetBillAmount {

	private Double amount;
	private String currencyCode;
	
	public NetBillAmount() {
		super();
	}

	public NetBillAmount(Double amount, String currencyCode) {
		super();
		this.amount = amount;
		this.currencyCode = currencyCode;
	}

	@Override
	public String toString() {
		return "NetBillAmount [amount=" + amount + ", currencyCode=" + currencyCode + "]";
	}
	
}
