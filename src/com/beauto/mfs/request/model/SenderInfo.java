package com.beauto.mfs.request.model;

import java.util.Date;

import org.springframework.stereotype.Component;

import lombok.Data;
@Data
public class SenderInfo {
	
	
	private String senderAddress;
	private String senderCity;
	private Date senderDob;
	private String idNumber;
	private String idType;
	private String idCountry;
	private Date idExpiry;
	private String email;
	private String fromCountry;
	private String msisdn;
	private String firstName;
	private String lastName;
	private String state;
	private String postalcode;
	
	public SenderInfo() {
		super();
	}

	public SenderInfo(String senderAddress, String senderCity, Date senderDob, String idNumber, String idType,
			String idCountry, Date idExpiry, String email, String fromCountry, String msisdn, String firstName,
			String lastName, String state, String postalcode) {
		super();
		this.senderAddress = senderAddress;
		this.senderCity = senderCity;
		this.senderDob = senderDob;
		this.idNumber = idNumber;
		this.idType = idType;
		this.idCountry = idCountry;
		this.idExpiry = idExpiry;
		this.email = email;
		this.fromCountry = fromCountry;
		this.msisdn = msisdn;
		this.firstName = firstName;
		this.lastName = lastName;
		this.state = state;
		this.postalcode = postalcode;
	}

	@Override
	public String toString() {
		return "SenderInfo [senderAddress=" + senderAddress + ", senderCity=" + senderCity + ", senderDob=" + senderDob
				+ ", idNumber=" + idNumber + ", idType=" + idType + ", idCountry=" + idCountry + ", idExpiry="
				+ idExpiry + ", email=" + email + ", fromCountry=" + fromCountry + ", msisdn=" + msisdn + ", firstName="
				+ firstName + ", lastName=" + lastName + ", state=" + state + ", postalcode=" + postalcode + "]";
	}
	
}
