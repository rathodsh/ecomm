package com.beauto.mfs.request.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Setter
@Getter
public class Corporate {

	private String corporateCode;
	private String password;
	
	public Corporate() {
		super();
	}

	public Corporate(String corporateCode, String password) {
		super();
		this.corporateCode = corporateCode;
		this.password = password;
	}

	@Override
	public String toString() {
		return "Corporate [corporateCode=" + corporateCode + ", password=" + password + "]";
	}
	
}


