package com.beauto.mfs.request.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Setter
@Getter
public class PaymentRequestModel {
	private String description;
	private String maerchantAccount;
	private Double taxRate;
	private String thirdPartyTransId;
	private String mfsSign;
	private GrossBillAmount grossBillAmount;
	private NetBillAmount netBillAmount;
	private Corporate corporate;
	private SenderInfo senderInfo;
	
	public PaymentRequestModel() {
		super();
	}

	public PaymentRequestModel(String description, String maerchantAccount, Double taxRate, String thirdPartyTransId,
			String mfsSign, GrossBillAmount grossBillAmount, NetBillAmount netBillAmount, Corporate corporate,
			SenderInfo senderInfo) {
		super();
		this.description = description;
		this.maerchantAccount = maerchantAccount;
		this.taxRate = taxRate;
		this.thirdPartyTransId = thirdPartyTransId;
		this.mfsSign = mfsSign;
		this.grossBillAmount = grossBillAmount;
		this.netBillAmount = netBillAmount;
		this.corporate = corporate;
		this.senderInfo = senderInfo;
	}

	@Override
	public String toString() {
		return "PaymentRequestModel [description=" + description + ", maerchantAccount=" + maerchantAccount
				+ ", taxRate=" + taxRate + ", thirdPartyTransId=" + thirdPartyTransId + ", mfsSign=" + mfsSign
				+ ", grossBillAmount=" + grossBillAmount + ", netBillAmount=" + netBillAmount + ", corporate="
				+ corporate + ", senderInfo=" + senderInfo + "]";
	}
	
}
