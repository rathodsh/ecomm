package com.beauto.mfs.request.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Setter
@Getter
public class GrossBillAmount {
	private Double amount;
	private String currencyCode;
	
	
	public GrossBillAmount() {
		super();
	}


	public GrossBillAmount(Double amount, String currencyCode) {
		super();
		this.amount = amount;
		this.currencyCode = currencyCode;
	}


	@Override
	public String toString() {
		return "GrossBillAmount [amount=" + amount + ", currencyCode=" + currencyCode + "]";
	}
	
	
}
