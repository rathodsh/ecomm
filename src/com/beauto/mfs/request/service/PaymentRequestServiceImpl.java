package com.beauto.mfs.request.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.beauto.mfs.request.dto.PaymentRequestDto;
import com.beauto.mfs.request.model.PaymentRequestModel;

@Service
public class PaymentRequestServiceImpl implements PaymentRequestService{

	@Autowired
	PaymentRequestDto requestDto;
	
	@Override
	public void requestPayment(PaymentRequestModel model) {
		requestDto.requestPayment(model);
	}

}
