package com.beauto.mfs.request.service;

import com.beauto.mfs.request.model.PaymentRequestModel;

public interface PaymentRequestService {

	public void requestPayment(PaymentRequestModel model);
}
