package com.beauto.mfs.request.dto;

import com.beauto.mfs.request.model.PaymentRequestModel;

public interface PaymentRequestDto {

	public void requestPayment(PaymentRequestModel model);
}
