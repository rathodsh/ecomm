package com.beauto.mfs.request.dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.beauto.mfs.request.model.PaymentRequestModel;
import com.mysql.cj.xdevapi.SessionFactory;

@Repository
public class PaymentRequestDtoImpl implements PaymentRequestDto {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public void requestPayment( PaymentRequestModel model) {
		System.out.println(model);
	}

}
