package com.beauto.mfs.request.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.beauto.mfs.request.model.PaymentRequestModel;
import com.beauto.mfs.request.service.PaymentRequestService;

@Controller
@RequestMapping("/payment")
public class PaymentRequestControllerImpl {

	PaymentRequestService service;
	
	 @GetMapping(value = "/request")
	public ResponseEntity<Object> requestPayment(@RequestBody PaymentRequestModel model){
		 
		 service.requestPayment(model);
		 System.out.println("hi");
		 
		 URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                 .path("/{id}")
                 .build()
                 .toUri();
		 
		 return ResponseEntity.created(location).build();
	}
}
